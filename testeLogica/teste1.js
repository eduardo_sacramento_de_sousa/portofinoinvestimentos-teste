exports.contagemOcorrencias = function(listaNome) {
    var ocorrencias   = {};
    listaNome.forEach(nome => {
        var temporario = nome.toUpperCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "");;
        if(ocorrencias[temporario] == undefined)
            ocorrencias[temporario] = 0;
        ocorrencias[temporario] += 1;        
    })

    return ocorrencias;
};

