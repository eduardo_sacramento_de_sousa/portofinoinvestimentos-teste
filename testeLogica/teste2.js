exports.processamento = function(lista){

    return lista.map(function(linha){
        return linha.map(function(valorOriginal){
            var listaConsiderada = linha.filter(function(itemTeste){
                var result = Math.abs(itemTeste - valorOriginal);
                if(result == 0 || result == 1)
                    return itemTeste;
            }, valorOriginal);
            var soma = listaConsiderada.reduce((acumulador, valorAtual) => acumulador + valorAtual);
            return soma / listaConsiderada.length;
        });
    })
}